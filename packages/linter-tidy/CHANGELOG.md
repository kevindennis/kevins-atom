# Change Log

## [v2.2.0](https://github.com/AtomLinter/linter-tidy/tree/v2.2.0) (2016-08-01)

[Full Changelog](https://github.com/AtomLinter/linter-tidy/compare/v2.1.1...v2.2.0)

**Implemented enhancements:**

-   Update atom-linter to version 7.0.0 🚀 [\#65](https://github.com/AtomLinter/linter-tidy/pull/65) ([greenkeeperio-bot][])
-   Pass cwd to linter [\#70](https://github.com/AtomLinter/linter-tidy/pull/70) ([thtliife][])
-   Update CI config [\#63](https://github.com/AtomLinter/linter-tidy/pull/63) ([Arcanemagus][arcanemagus])
-   Update eslint to version 3.2.0 🚀 [\#72](https://github.com/AtomLinter/linter-tidy/pull/72) ([greenkeeperio-bot][])
-   Update eslint-config-airbnb-base to version 5.0.1 🚀 [\#72](https://github.com/AtomLinter/linter-tidy/pull/72) ([greenkeeperio-bot][])
-   Update eslint-plugin-import to version 1.12.0 🚀 [\#72](https://github.com/AtomLinter/linter-tidy/pull/72) ([greenkeeperio-bot][])
-   Update CI script [\#72](https://github.com/AtomLinter/linter-tidy/pull/72) ([Arcanemagus][arcanemagus])
-   Allow custom grammar scopes to be linted [\#71](https://github.com/AtomLinter/linter-tidy/pull/71) ([tyearke][])

[arcanemagus]: https://github.com/Arcanemagus
[greenkeeperio-bot]: https://github.com/greenkeeperio-bot
[thtliife]: https://github.com/thtliife
[tyearke]: https://github.com/tyearke
